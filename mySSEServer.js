class SSEServer {
    subscribers = new Set();
    sessions = new Map();

    constructor() {
    }

    addClient(request, response) {
        SSEServer.initConnection(request, response);
    
        this.subscribers.add(response);
        this.sessions.set(response, {});
    
        request.on("close", () => this.removeSubscriber(response))
    }

    static initConnection(request, response) {
        request.socket.setTimeout(0);

        response.writeHead(200, {
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive'
        });
    
        response.write('\n\n');
    }

    static sendEvent(res, id, event, data = "") {
        if (!id && !event && !data)
            return;

        console.log("bip bop", id, event, data);

        if (id != null) res.write(`id: ${id}\n`)
        if (event != null) res.write(`event: ${event}\n`)
        if (data != null) res.write(`data: ${JSON.stringify(data)}\n`)

        res.write("\n");
    }

    disconnectClient() {

    }

    removeSubscriber(res) {
        res.end();
        this.subscribers.delete(res);
        this.sessions.delete(res, {});
    }

    sendToAll(id, event, data) {
        console.log("sendToAll", id, event, data);
        //id = id ? id : Date.now();
        this.subscribers.forEach((res) => SSEServer.sendEvent(res, id, event, data));
    }
}

module.exports = SSEServer;